# minimal-pvpn-networkd-resolved

Automate picking an openvpn configuration for protonvpn, to work with networkd and resolved.

To use this for yourself follow these steps:

1. Ensure you have openvpn installed, and you are using systemd-networkd and systemd-resolved.
2. Install python3 and pycountry if you don't have it.
3. Set up your DNS settings in config file for networkd, leave resolved without specific settings to ensure DNS will be used from openvpn
4. Download protonvpn configs for openvpn
5. Place standard country configs in /etc/openvpn/client/country and make files readable to openvpn user
6. Place secure core configs in /etc/openvpn/client/sc and make files readable to openvpn user
7. Find your openvpn user/pass from your protonvpn settings page, put it in /etc/openvpn/client/pass.txt where the first line of the file is just your username, and the second line is just your pass. Nothing else in the file. The file should be readable by openvpn user, and nobody else.
8. Get https://github.com/jonathanio/update-systemd-resolved and put it in /usr/bin
9. Now you are good to go

Call this program with 'c' to connect, 'd' to disconnect. Combine 'd' and '-sc' to use Secure Core.
If this all seems too complicated for you, then I suggest you just use the official ProtonVPN app for Linux, it should work great on a standard setup.
