#!/usr/bin/env python3

import os, os.path, sys, pycountry, subprocess, shutil

def print_help():
    print("Minimal Protonvpn for systemd that use systemd-networkd and systemd-resolved")
    print("Basic usage options:")
    print("            c   - connect to a Protonvpn server with OpenVPN")
    print("            d   - disconnect")
    print("          -sc   - connect to a secure core server, for use with the 'c' option")

def byfirst(val):
    return val[0]

def bysecond(val):
    return val[1]

def get_countries(fpath):
    files = os.listdir(fpath)
    countrylist = []
    for file in files:
        if file[:2] == 'uk':
            cc = 'United Kingdom'
        else:
            cc = pycountry.countries.get(alpha_2=file[:2]).name
        countrylist.append((cc, file))
    countrylist.sort(key = byfirst)
    return countrylist

def get_sc_countries(fpath):
    files = os.listdir(fpath)
    countrylist = []
    for file in files:
        if file[3:5] == 'uk':
            cc = 'United Kingdom'
        else:
            cc = pycountry.countries.get(alpha_2=file[3:5]).name
        via = pycountry.countries.get(alpha_2=file[:2]).name
        countrylist.append((cc + " via " + via, file))
    countrylist.sort(key = bysecond)
    countrylist.sort(key = byfirst)
    return countrylist

if os.getuid() != 0:
    print("You need to run this as root")
    sys.exit()

secure_core = False
connect = False
disconnect = False
cpath = "/etc/openvpn/client/country"

number_of_args = len(sys.argv)
if number_of_args < 2:
    print("Mising arguments")

for i in range(1, number_of_args):
    if sys.argv[i] == '-sc':
        secure_core = True
        cpath = "/etc/openvpn/client/sc"
    elif sys.argv[i] == 'c':
        connect = True
    elif sys.argv[i] == 'd':
        disconnect = True
    elif sys.argv[i] == '--help' or sys.argv[i] == '-h':
        print_help()
    else:
        print("Unknown argument: " + sys.argv[i])
        sys.exit()

vpn_on = bool(subprocess.run(('pgrep', '--exact', 'openvpn'), stdout=subprocess.PIPE).stdout)
conf_path = "/etc/openvpn/client/protonvpn.conf"

if connect and disconnect:
    print("Can't both connect and disconnect")
    sys.exit()
elif connect and vpn_on:
    print("OpenVPN is already running.")
    sys.exit()
elif disconnect and not vpn_on:
    print("OpenVPN is not running.")
    sys.exit()
elif connect and not vpn_on:
    if secure_core:
        clist = get_sc_countries(cpath)
    else:
        clist = get_countries(cpath)
    for i in range(len(clist)):
        print(str(i+1) + ". " + clist[i][0])
    selection = False
    while not selection:
        snumber = input("Select your server: ")
        if snumber.isdecimal():
            snumber = int(snumber)
            if snumber > 0 and snumber <= len(clist):
                selection = True

    snumber -= 1

    fd = open(os.path.join(cpath, clist[snumber][1]), "r")
    contents = fd.read()
    fd.close()

    contents = contents.replace("auth-user-pass", "auth-user-pass pass.txt")
    contents = contents.replace("script-security 2", "dhcp-option DOMAIN-ROUTE .\nscript-security 2")
    contents = contents.replace("up /etc/openvpn/update-resolv-conf", "setenv PATH /usr/bin\nup /usr/bin/update-systemd-resolved")
    contents = contents.replace("down /etc/openvpn/update-resolv-conf", "down /usr/bin/update-systemd-resolved\ndown-pre")

    fd = open(conf_path, "w")
    fd.write(contents)
    fd.close()

    shutil.chown(conf_path, "openvpn", "network")

    # Disable ipv6 and start openvpn
    subprocess.run("sysctl net.ipv6.conf.all.disable_ipv6=1 1>/dev/null", shell=True)
    subprocess.run("systemctl start openvpn-client@protonvpn.service", shell=True)
elif disconnect and vpn_on:
    # Disable openvpn and enable ipv6
    subprocess.run("systemctl stop openvpn-client@protonvpn.service", shell=True)
    subprocess.run("sysctl net.ipv6.conf.all.disable_ipv6=0 1>/dev/null", shell=True)
    subprocess.run("systemctl restart systemd-networkd.service", shell=True)
